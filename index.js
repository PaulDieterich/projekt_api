var express = require("express");
var app = express();
var bodyParse = require("body-parser");
var db = require("./models");

var projektRoutes = require("./routes/projekte");
app.set('view engine', 'ejs');  
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({extended: true}));

app.get("/", function(req, res){
    db.Projekt.find()
    .then(function(projekt){
        res.render("pages/view", {projekt: projekt});
       
    })
   
})
app.use("/api/projekt", projektRoutes);

app.listen(3000, function(){
    console.log(" server on localhost:3000");
})