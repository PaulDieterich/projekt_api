var db = require("../models");

exports.getProjekte = function(req, res){
    db.Projekt.find()
    .then(function(projekt){
        res.json(projekt);
    })
    .catch(function(err){
        res.setEncoding(err);
    })
}

exports.createProjekt=  function(req, res){
    db.Projekt.create(req.body)
    .then(function(newProjekt){
      res.status(201).json(newProjekt);
    })
    .catch(function(err){
        res.send(err);
    })  
}
exports.getProjekt =  function(req, res){
    db.Projekt.findById(req.params.ProjektId)
    .then(function(findProjekt){
        res.json(findProjekt);
    })
    .catch(function(err){
        res.send(err);
    })
}
exports.updateProjekt = function(req, res){
    db.Projekt.findOneAndUpdate({_id: req.params.ProjektId}, req.body, {new: true})
    .then(function(projekt){
        res.json(projekt);
    })
    .catch(function(err){
        res.send(err);
    })
}
exports.deleteProjekt = function(req, res){
    db.Projekt.remove({_id: req.params.ProjektId})
    .then(function(projekt){
        res.json({massage: "We deleted it!"});
    })
    .catch(function(err){
        res.send(err);
    })
}

module.exports = exports;