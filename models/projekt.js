var mongoose = require("mongoose");

var projektSchema = new mongoose.Schema({
    name:{
        type: String,
        required: "Name cannot be blank!"
    },
    subName:{
        type:String 
    },
    text: {
        type: String
    },
    created_data:{
        type: Date,
        default: Date.now
    },
    img:{
        type: String
    },
    tags:{
        type: Array
    }
});

var Projekt = mongoose.model("Projekt", projektSchema);

module.exports = Projekt;