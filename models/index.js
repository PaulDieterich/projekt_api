var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/projekt-api", { useNewUrlParser: true });

mongoose.Promise = Promise;

module.exports.Projekt = require("./projekt")