var express = require("express");
var router = express.Router();

var helper = require("../helpers/projekt")

router.route("/")
    .get(helper.getProjekte)
    .post(helper.createProjekt)

router.route("/:projektId")
    .get(helper.getProjekt)
    .get(helper.updateProjekt)
    .get(helper.deleteProjekt)

module.exports = router;